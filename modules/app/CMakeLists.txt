cmake_minimum_required(VERSION 3.10)

project(app LANGUAGES CXX)

set(SOURCES
	src/Main.cpp

	src/handlers/Check.cpp
	src/handlers/Default.cpp

	src/HttpServer.cpp
	src/RequestRouter.cpp
	src/ServerApp.cpp
)

add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		${CONAN_LIBS}
		monitor
)

target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
target_compile_options(${PROJECT_NAME}
	PRIVATE
		$<$<CXX_COMPILER_ID:MSVC>:
			/MP /W3 /Zf
			$<$<CONFIG:Debug>:/MTd>
			$<$<CONFIG:Release>:/MT>>
		$<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:GNU>>:
			-Wall -Wextra -Werror -Wpedantic -pedantic-errors -pipe>
)
