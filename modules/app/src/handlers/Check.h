#pragma once

namespace app
{

class Check : public Poco::Net::HTTPRequestHandler
{
	//
	// Public interface.
	//
public:
	//! Handles all invalid uri paths.
	void handleRequest(Poco::Net::HTTPServerRequest& request,
		Poco::Net::HTTPServerResponse& response) override;
};

}	// namespace app
