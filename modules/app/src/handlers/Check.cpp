#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include <monitor/PrometheusMetrics.h>

#include "Check.h"

namespace app
{

void Check::handleRequest(Poco::Net::HTTPServerRequest&,
	Poco::Net::HTTPServerResponse& response)
{
	monitor::PrometheusMetrics::Singleton()
		.GetHttp().Increment(monitor::HttpRequestFamily::Counters::HTTP_CHECK);

	monitor::PrometheusMetrics::Singleton()
		.GetHttp().Increment(monitor::HttpRequestFamily::Gauges::HTTP);

	response.send() << "What's good in da hood but checked" << std::endl;
}

}	// namespace app
