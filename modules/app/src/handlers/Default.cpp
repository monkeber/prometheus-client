#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include <monitor/PrometheusMetrics.h>

#include "Default.h"

namespace app
{

void Default::handleRequest(Poco::Net::HTTPServerRequest&,
	Poco::Net::HTTPServerResponse& response)
{
	monitor::PrometheusMetrics::Singleton()
		.GetHttp().Increment(monitor::HttpRequestFamily::Counters::HTTP_TOTAL);

	monitor::PrometheusMetrics::Singleton()
		.GetHttp().Decrement(monitor::HttpRequestFamily::Gauges::HTTP);

	response.send() << "What's good in da hood" << std::endl;
}

}	// namespace app
