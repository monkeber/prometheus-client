#pragma once

namespace app
{

class HttpServer
{
	//
	// Constructor and destructor.
	//
public:
	//! Constructor.
	HttpServer(const std::string& host, const uint16_t port);

	//
	// Public interface.
	//
public:
	//! Starts the server. New thread will be created that waits for and accepts incoming
	//! connections.
	void Start();
	//! Stops the server. No new connections will be accepted.
	void Stop();

	//
	// Private data members.
	//
private:
	//! Full-featured multithreaded HTTP server.
	std::unique_ptr<Poco::Net::HTTPServer> httpServer_ = nullptr;
	//! Interface to a TCP server socket.
	std::unique_ptr<Poco::Net::ServerSocket> socket_ = nullptr;
};

}	// namespace app
