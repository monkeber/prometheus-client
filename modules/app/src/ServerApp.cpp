#include <iostream>
#include <memory>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Util/ServerApplication.h>

#include "HttpServer.h"
#include "ServerApp.h"

namespace app
{

void ServerApp::defineOptions(Poco::Util::OptionSet& options)
{
	Poco::Util::ServerApplication::defineOptions(options);

	Poco::Util::Option configOpt{ "config", "c", "Path to the configuration settings file" };
	configOpt.repeatable(false);
	configOpt.argument("filename");
	options.addOption(configOpt);

	Poco::Util::Option helpOpt{ "help", "h", "Display help info on command line arguments" };
	helpOpt.repeatable(false);
	options.addOption(helpOpt);

	Poco::Util::Option cronOpt{ "task", "t", "Task type for cron" };
	cronOpt.repeatable(false);
	cronOpt.argument("type");
	options.addOption(cronOpt);
}

void ServerApp::handleOption(const std::string& name, const std::string& value)
{
	Poco::Util::ServerApplication::handleOption(name, value);
}

void ServerApp::initialize(Poco::Util::Application& self)
{
	Poco::Util::ServerApplication::initialize(self);
}

void ServerApp::uninitialize()
{
	Poco::Util::ServerApplication::uninitialize();
}

int ServerApp::main(const std::vector<std::string>&)
{
	const std::string host{ "0.0.0.0" };
	constexpr uint16_t port{ 8080 };

	app::HttpServer httpServer{ host, port };
	httpServer.Start();

	std::cout << "Server is up" << std::endl;

	// Wait for CTRL-C or kill.
	waitForTerminationRequest();

	std::cout << "Shutting down the server..." << std::endl;
	httpServer.Stop();

	return 0;
}

}	// namespace app
