#pragma once

namespace app
{

class RequestRouter : public Poco::Net::HTTPRequestHandlerFactory
{
	//
	// Public interface.
	//
public:
	//! Creates a new request handler for the given HTTP request.
	Poco::Net::HTTPRequestHandler* createRequestHandler(
		const Poco::Net::HTTPServerRequest& request) override;
};

}	// namespace app
