#include <iostream>
#include <memory>
#include <string>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Util/ServerApplication.h>

#include "HttpServer.h"
#include "RequestRouter.h"

namespace app
{

HttpServer::HttpServer(const std::string& host, const uint16_t port)
{
	constexpr int queueSize = 64;
	constexpr int threads = 8;

	auto params = new Poco::Net::HTTPServerParams{};
	params->setMaxQueued(queueSize);
	params->setMaxThreads(threads);

	socket_.reset(new Poco::Net::ServerSocket{
		Poco::Net::SocketAddress{ Poco::Net::IPAddress{ host }, port } });

	httpServer_.reset(new Poco::Net::HTTPServer{ new RequestRouter{}, *socket_, params });

	std::cout << "Server running on: " << host << ':' << port << std::endl;
}

void HttpServer::Start()
{
	httpServer_->start();
}

void HttpServer::Stop()
{
	httpServer_->stop();
}

}	// namespace app
