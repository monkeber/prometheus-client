#include <iostream>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Util/ServerApplication.h>

#include "ServerApp.h"

POCO_SERVER_MAIN(app::ServerApp)
