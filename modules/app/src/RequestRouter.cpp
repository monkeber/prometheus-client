#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/URI.h>

#include "handlers/Check.h"
#include "handlers/Default.h"
#include "RequestRouter.h"

namespace app
{

Poco::Net::HTTPRequestHandler* RequestRouter::createRequestHandler(
	const Poco::Net::HTTPServerRequest& request)
{
	const auto uri = Poco::URI { request.getURI() };
	const auto path = uri.getPath();
	const auto method = request.getMethod();

	if (path == "/check")
	{
		return new Check{};
	}
	else
	{
		return new Default{};
	}
}

}	// namespace app
