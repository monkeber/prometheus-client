#pragma once

namespace app
{

class ServerApp : public Poco::Util::ServerApplication
{
	//
	// Construction and destruction.
	//
public:
	//! Constructor.
	ServerApp() = default;

	//
	// Protected methods.
	//
public:
	//! Defines command line arguments.
	void defineOptions(Poco::Util::OptionSet& options) override;
	//! Processes command line argument with the given name.
	void handleOption(const std::string& name, const std::string& value) override;
	//! Initializes the application and all registered subsystems.
	void initialize(Poco::Util::Application& self) override;
	//! Performs the application's main logic.
	int main(const std::vector<std::string>&) override;
	//! Deinitializes the application and all registered subsystems.
	void uninitialize() override;
};

}	// namespace app
