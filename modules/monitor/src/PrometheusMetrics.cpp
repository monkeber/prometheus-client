#include "monitor/PrometheusMetrics.h"

namespace monitor
{

PrometheusMetrics::PrometheusMetrics()
	: exposer_{ "0.0.0.0:8090" }
	, registry_{ std::make_shared<prometheus::Registry>() }
	, httpRequests_{ registry_ }
{
	exposer_.RegisterCollectable(registry_);
}

HttpRequestFamily& PrometheusMetrics::GetHttp()
{
	return httpRequests_;
}

PrometheusMetrics& PrometheusMetrics::Singleton()
{
	static PrometheusMetrics instance;

	return instance;
}

}	// namespace monitor
