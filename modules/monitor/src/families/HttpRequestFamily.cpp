#include "monitor/families/HttpRequestFamily.h"

namespace monitor
{

HttpRequestFamily::HttpRequestFamily(std::shared_ptr<prometheus::Registry> registry)
	: familyCounters_{ prometheus::BuildCounter()
		.Name("http_requests_total")
		.Help("How many requests has this server accepted?")
		.Register(*registry) }
	, familyGauge_{ prometheus::BuildGauge()
		.Name("incrementation")
		.Help("Summary of calls to increment and decrement")
		.Register(*registry)
	}
	, checkRequests_{ familyCounters_.Add({{ "handler", "check" }}) }
	, gauge_{ familyGauge_.Add({{ "gauge", "value" }}) }
	, totalRequests_{ familyCounters_.Add({{ "handler", "default" }}) }
{
}

void HttpRequestFamily::Decrement(const Gauges endpoint)
{
	if (endpoint == Gauges::HTTP)
	{
		gauge_.Decrement();
	}
}

void HttpRequestFamily::Increment(const Counters endpoint)
{
	if (endpoint == Counters::HTTP_TOTAL)
	{
		totalRequests_.Increment();
	}
	else if (endpoint == Counters::HTTP_CHECK)
	{
		checkRequests_.Increment();
	}
}

void HttpRequestFamily::Increment(const Gauges endpoint)
{
	if (endpoint == Gauges::HTTP)
	{
		gauge_.Increment();
	}
}

}	// namespace monitor
