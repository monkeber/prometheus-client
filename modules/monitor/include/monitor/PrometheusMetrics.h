#pragma once

#include <memory>

#include <prometheus/exposer.h>
#include <prometheus/registry.h>

#include "families/HttpRequestFamily.h"

namespace monitor
{

class PrometheusMetrics
{
	//
	// Public interface.
	//
public:
	//! Returns metric for http requests.
	HttpRequestFamily& GetHttp();
	//! Returns reference to the static Prometheus object.
	static PrometheusMetrics& Singleton();

	//
	// Construction and destruction.
	//
private:
	PrometheusMetrics();

	//
	// Private data members.
	//
private:
	//! Object to expose metrics.
	prometheus::Exposer exposer_;
	//! Prometheus registry.
	std::shared_ptr<prometheus::Registry> registry_;
	//! Family of counters to represent metrics related to http requests.
	HttpRequestFamily httpRequests_;
};

}	// namespace monitor
