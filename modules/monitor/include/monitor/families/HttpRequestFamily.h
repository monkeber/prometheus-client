#pragma once

#include <string>
#include <unordered_map>

#include <prometheus/registry.h>

namespace monitor
{

class HttpRequestFamily
{
	//
	// Enum types.
	//
public:
	//! Can only grow.
	enum class Counters
	{
		HTTP_TOTAL,
		HTTP_CHECK
	};
	//! Can increase and decrease.
	enum class Gauges
	{
		HTTP
	};

	//
	// Construction and destruction.
	//
public:
	//! Constructor.
	HttpRequestFamily(std::shared_ptr<prometheus::Registry> registry);
	//! Constructor.
	HttpRequestFamily(const HttpRequestFamily&) = delete;
	//! Move constructor.
	HttpRequestFamily(const HttpRequestFamily&&) = delete;
	//! Assignment operator.
	HttpRequestFamily& operator=(const HttpRequestFamily&) = delete;
	//! Move operator.
	HttpRequestFamily& operator=(const HttpRequestFamily&&) = delete;

	//
	// Public interface.
	//
public:
	//! Decrements gauges.
	void Decrement(const Gauges endpoint)
;	//! Increments counters representing accepted requests to a default endpoint.
	void Increment(const Counters endpoint);
	//! Increments gauges.
	void Increment(const Gauges endpoint);

	//
	// Private data members.
	//
private:
	//! Counter family.
	prometheus::Family<prometheus::Counter>& familyCounters_;
	//! Gauge family.
	prometheus::Family<prometheus::Gauge>& familyGauge_;
	//! Represents total number of accepted requests to check.
	prometheus::Counter& checkRequests_;
	//! Gauge to test incrementing and decrementing.
	prometheus::Gauge& gauge_;
	//! Represents total number of accepted requests.
	prometheus::Counter& totalRequests_;
};

}	// namespace monitor
