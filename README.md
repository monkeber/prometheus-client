# Prometheus client

This project provides very simple example of http server using client library for Prometheus metrics along with Grafana.

# Table Of Contents

- [Plots Example](#plots)
- [How To Build](#how-to-build)


<a name="plots"/>

# Plots Example

Here is an example of two plots you can build using this project.

![Imgur](https://i.imgur.com/D9ueoGp.png)

<a name="how-to-build"/>

# How To Build

Requirements:
- Docker
- CMake (only for non-docker build)
- Conan package manager (only for non-docker build)
- GCC, tested on version 8.2 (only for non-docker build)

Build using docker (along with Grafana and Prometheus itself):
1. Don't forget to initialize all project submodules.
1. Go to `prometheus` directory and run:
	```
	docker-compose build
	docker-compose up
	```
1. Now you can access following targets:

	- The server itself at `localhost:8080`
	- Exposer used by the server at `localhost:8090`
	- Prometheus at `localhost:9090`
	- Grafana at `localhost:3000` (login is `admin`, password is `qweqwe`)

Build without docker (only server, Prometheus and Grafana will not be accessible after this build, unless you have installed and configured them by yourself):

1. Go to the directory `scripts`
1. Run one of the build-* scripts
1. Run the `app` in `${repository}/build/gcc-*/bin`
1. Now you can access following targets:

	- The server itself on `localhost:8080`
	- Exposer used by the server at `localhost:8090`
