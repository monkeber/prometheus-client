# Base image.
FROM ubuntu:bionic

#
# C++ development environment configuration
#

# Update system to the newest versions of packages and their dependencies.
RUN apt-get update
# Install development packages.
RUN apt-get install -y git build-essential autoconf libtool cmake python-dev pkg-config gcc-8 g++-8
# Special handling for tzdata package.
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install tzdata
RUN ln -sf /usr/share/zoneinfo/GMT /etc/localtime

RUN apt install -y python3-pip

# Install the latest Conan.
RUN pip3 install -q --no-cache-dir conan
RUN conan user
RUN conan --version
# Configuring Conan repositories.
RUN conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan

# Set working directory.
WORKDIR /app

# Bundle app sources.
COPY . ./

# Build app.
ENV CXX /usr/bin/g++-8
RUN ./scripts/build-gcc-x64-release

ENTRYPOINT [ "/app/build/gcc-x64-Release/bin/app" ]
